package com.example.dadata;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import ru.dadata.api.entity.Address;

import java.net.UnknownHostException;
import java.util.Scanner;

@SpringBootApplication
public class DadataApplication implements CommandLineRunner {

    public static void main(String[] args) throws UnknownHostException {

        SpringApplication.run(DadataApplication.class, args);
    }

    //@Override
    public void run(String... args) throws Exception {
        System.out.println("Input address");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        MainContorller.startPage(str);
    }
}
